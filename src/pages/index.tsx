import React from 'react'
import {
    BrowserRouter as Router,
    Switch,
    Route
} from 'react-router-dom'

import InputPage from './InputPage'
import GamePage from './GamePage'
import ResultPage from './ResultPage'


const App = () => {
    return (
        <Router>
            <Switch>
                <Route path="/game" component={GamePage}/>
                <Route path="/result" component={ResultPage}/>
                <Route path="/" component={InputPage}/>
            </Switch>
        </Router>
    )
}

export default App