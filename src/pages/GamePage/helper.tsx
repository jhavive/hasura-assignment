export interface counterObject{
    x: number,
    o: number
}

export enum player {
    X = "x",
    O = "o"
}

export const arrayGenerator = (n: number) => {
    let arr = []
    for(let i=0;i<n;i++){
        arr.push({
            x: 0,
            o: 0
        })
    }
    return arr
}

export const checkGameStatus = (
    n: number, counter: number, i: number, j: number,
    rowsCount:Array<counterObject>, columnCount: Array<counterObject>, diagonalCount: Array<counterObject>
    ) => {
    

    // for (let i=0; i< rowsCount.length;i++){
        let row = rowsCount[i]
        if(row[player.X]===n){
            return player.X
        } else if (row[player.O]===n){
            return player.O
        } 
    // }

    // for (let i=0; i< columnCount.length;i++){
        let column = columnCount[j]
        if(column[player.X]===n){
            return player.X
        } else if (column[player.O]===n){
            return player.O
        } 
    // }

    if(diagonalCount[0][player.X]===n){
        return player.X
    } else if (diagonalCount[0][player.O]===n){
        return player.O
    } 
    
    if(diagonalCount[1][player.X]===n){
        return player.X
    } else if (diagonalCount[1][player.O]===n){
        return player.O
    } 

    if(counter === n * n )
        return "draw"
}
