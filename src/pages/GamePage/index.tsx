import { useState } from "react"
import React from 'react';
import './styles.css'
import {
    counterObject,
    player,
    arrayGenerator,
    checkGameStatus,
} from './helper'


const GamePage = (props: any) => {

    if(!props.location.state.n){
        props.history.push('/')
    }
    let n = props.location.state.n

    let [chance, setChance] = useState(player.X)
    let [rowsCount, setRowsCount] = useState<Array<counterObject>>(arrayGenerator(n))
    let [columnCount, setColumnCount] = useState<Array<counterObject>>(arrayGenerator(n))
    let [diagonalCount, setDiagonalCount] = useState<Array<counterObject>>(arrayGenerator(2))
    let [counter, setCounter] = useState<number>(1)

    const gridClick = (e:any) => {
        let element = e.target
        if(element.classList.contains(player.X) || element.classList.contains(player.X) )
            return
        let index = element.dataset.index.split(",")

        

        element.classList.add(chance)

        setCounter(counter+1)

        rowsCount[index[0]][chance]++
        setRowsCount(rowsCount)

        columnCount[index[1]][chance]++
        setColumnCount(columnCount)

        if(index[0]===index[1]){
            diagonalCount[0][chance]++
            setDiagonalCount(diagonalCount)
        }
        // console.log("reverse diagonal", index[0], index[1] ,( index[0] + index[1]), (n - 1))
        if(parseInt(index[0]) + parseInt(index[1]) == (n - 1)){
            diagonalCount[1][chance]++
            setDiagonalCount(diagonalCount)
        }

        if(counter > n){
            let gameState = checkGameStatus(n, counter, parseInt(index[0]),parseInt(index[1]),rowsCount, columnCount, diagonalCount)
            console.log(rowsCount, columnCount, diagonalCount)
            switch(gameState){
                case player.X:
                case player.O:
                    let obj = 
                    props.history.push({
                        pathname: "/result",
                        state: {
                            gameState: "win",
                            player: gameState
                        }
                    })
                    break
                case "draw":
                    props.history.push({
                        pathname: "/result",
                        state: {
                            gameState: "draw",
                        }
                    })
                    break
                default:
                    break
                    
            }
        }
        if (chance === player.X)
            setChance(player.O)
        else
            setChance(player.X)
    }

    let arrRow: Array<string>[] = []
    for(let i=0;i<n;i++){
        let arrColumn: string[] = []
        for(let j=0;j<n;j++){
            arrColumn.push("")
        }
        arrRow.push(arrColumn)
    }

    return (
        <div className="grid" onClick={gridClick}>
            { 
                arrRow.map((row, i) => <div className="row" >
                    {
                        row.map((cell, j) => <div className="cell" data-index={i+","+j}></div>)
                    }
                </div>)
            }
        </div>
    )
}

export default GamePage;