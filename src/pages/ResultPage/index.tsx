import React from 'react';

const ResultPage = (props: any) => {
    console.log(props.location.state)

    return <div>{` ${('player' in props.location.state)? props.location.state.player.toUpperCase()+' wins!!!' : 'Draw'}`}
        <br/>
        <br/>
        <br/>
        <button onClick={e => props.history.push('/')}>Restart</button>
    </div>
}

export default ResultPage;