import { useState } from "react"
import React from 'react';

const InputPage = (props: any) => {
    let [input, setInput] = useState<number>(0)

    return (
        <React.Fragment>
            <input onChange={e => setInput(parseInt(e.target.value) || 0)} value={input}/>
            <button onClick={e => props.history.push({
                pathname: '/game',
                state: { n: input }
            })}>Submit</button>
        </React.Fragment>
    )
}

export default InputPage;